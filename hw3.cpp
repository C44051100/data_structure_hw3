#include<iostream>
#include<iomanip>
#include<string>

using namespace std;

class Customer{
    friend class Quenue;
public:
    Customer(){
        this->name="";
        this->group=0;
    }
    string name;
    int group;
};

class Queue{
public:
    Queue(int size){
        max_size=size;
        now=0;
        sptr=new Customer[size];
    }

    void in(Customer input){
        if(this->now==0){
            this->sptr[0]=input;
            this->now++;
            return;
        }
        if(input.group==0){
            this->sptr[now]=input;
            this->now++;
            return;
        }
        for(int i=0;i<this->now;i++){
            if(input.group==this->sptr[i].group){
                for(int j=now;j>=i;j--){
                    this->sptr[j+1]=this->sptr[j];
                }
                this->sptr[i]=input;
                break;
            }
            if(i==this->now-1)this->sptr[now]=input;
        }
        this->now++;
    }
    string out(){
        string temp=this->sptr[0].name;
        for(int i=0;i<this->now;i++){
            this->sptr[i]=this->sptr[i+1];
        }
        this->now--;
        return temp;
    }
    bool is_full(){
        if(now==max_size)return true;
        else return false;
    }
    bool is_empty(){
        if(now==0)return true;
        else return false;
    }
    Customer *sptr;
    int now;
private:
    int max_size;
    
    
};

int main(){

    int group_num=0; //number of the groups
    int poeple_num=0; //number of people in one group
    int total_people_num=0;

    string act;
    string item;

    cin>>group_num;
    Customer customer[group_num*26];
    string output[group_num*26]={""};
    int output_count=0;

    for(int i=0;i<group_num;i++){
        cin>>poeple_num;
        
        for(int j=total_people_num;j<total_people_num+poeple_num;j++){
            cin>>customer[j].name;
            customer[j].group=i+1;
            
        }
        total_people_num+=poeple_num;
    }


    Queue queue(total_people_num*2);

    while(!cin.eof()){
        cin>>act;
        if(act.compare("ENQUEUE")==0){
            cin>>item;
            for(int i=0;i<total_people_num;i++){
                if(item.compare(customer[i].name)==0){
                    queue.in(customer[i]);
                    break;
                }
                if(i==total_people_num-1){
                    customer[total_people_num].name=item;
                    customer[total_people_num].group=0;
                    total_people_num++;
                }
            }
            
        }
        //cout<<queue.sptr[0].name<<queue.now<<endl;
        if(act.compare("DEQUEUE")==0){
            if(!queue.is_empty()){
                output[output_count]=queue.out();
                output_count++;
            }
        }
        
    }

    for(int i=0;i<output_count;i++){
        cout<<output[i]<<endl;
    }

    return 0;
}
